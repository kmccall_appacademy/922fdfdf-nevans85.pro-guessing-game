# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  target = Random.rand(1..100)
  guess_count = 0
  guess =  nil
  until guess == target
    guess = input_guess
    evaluate_guess(guess, target)
    guess_count += 1
  end
  puts("You got it in #{guess_count} guesses")
end

def input_guess
  puts 'Guess a Number'
  guess = gets.chomp
  guess.to_i
end

def evaluate_guess(guess, winner)
  puts("#{guess} is too high") if guess > winner
  puts("#{guess} is too low") if guess < winner
  puts("#{guess}, Thats It!") if guess == winner
end

def file_shuffler
  filename = input_filename
  content = File.readlines(filename)
  File.open("#{filename}-shuffled.txt", 'w') do |f|
    f.puts(content.shuffle)
  end
end

def input_filename
  puts 'Which file to shuffle?'
  gets.chomp
end
